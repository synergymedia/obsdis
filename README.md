# Obsdis - Observatorio de la Discapacidad
Este es un prototipo descartable para el Observatorio de la Discapacidad. Esto implica que aún no está terminado, no es funcional y no se recomienda utilizar en entornos productivos. 

# Licencia 
![AGPL Licensed](https://img.shields.io/badge/License-AGPLv3-informational?logo=gnu) ![AGPL](https://www.gnu.org/graphics/agplv3-155x51.png)

The obsdis software is under the GNU Affero General Public License version 3.

El software obsdis se encuenra bajo la licencia GNU Affero General Public License version 3. Para más información diríjase a https://www.gnu.org/licenses/agpl-3.0.html 

## Licencia de los diseños
![CC-By-SA logo](https://licensebuttons.net/l/by-sa/4.0/88x31.png)

Los diseños (archivos en la carpeta "design") se encuentran bajo la [licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/).
