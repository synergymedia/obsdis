<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LaborAsistencia;

/**
 * LaborAsistenciaSearch represents the model behind the search form of `app\models\LaborAsistencia`.
 */
class LaborAsistenciaSearch extends LaborAsistencia
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'asistente_id', 'con_discapacidad_id', 'formal', 'con_residencia'], 'integer'],
            [['desde', 'hasta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LaborAsistencia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'asistente_id' => $this->asistente_id,
            'con_discapacidad_id' => $this->con_discapacidad_id,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'formal' => $this->formal,
            'con_residencia' => $this->con_residencia,
        ]);

        return $dataProvider;
    }
}
