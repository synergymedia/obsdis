<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barrio".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $ciudad_id
 *
 * @property Ciudad $ciudad
 * @property Persona[] $personas
 */
class Barrio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barrio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ciudad_id'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['ciudad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudad::className(), 'targetAttribute' => ['ciudad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ciudad_id' => 'Ciudad ID',
        ];
    }

    /**
     * Gets query for [[Ciudad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    /**
     * Gets query for [[Personas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['barrio_id' => 'id']);
    }
}
