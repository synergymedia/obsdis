<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciudad".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $provincia_id
 *
 * @property Barrio[] $barrios
 * @property Provincia $provincia
 * @property Persona[] $personas
 * @property Persona[] $personas0
 */
class Ciudad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['provincia_id'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['provincia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provincia::className(), 'targetAttribute' => ['provincia_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'provincia_id' => 'Provincia ID',
        ];
    }

    /**
     * Gets query for [[Barrios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarrios()
    {
        return $this->hasMany(Barrio::className(), ['ciudad_id' => 'id']);
    }

    /**
     * Gets query for [[Provincia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia()
    {
        return $this->hasOne(Provincia::className(), ['id' => 'provincia_id']);
    }

    /**
     * Gets query for [[Personas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['lugar_nac' => 'id']);
    }

    /**
     * Gets query for [[Personas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas0()
    {
        return $this->hasMany(Persona::className(), ['vive_en' => 'id']);
    }
}
