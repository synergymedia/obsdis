<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Educacion;

/**
 * EducacionSearch represents the model behind the search form of `app\models\Educacion`.
 */
class EducacionSearch extends Educacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'institucion_educativa_id', 'con_discapacidad_id', 'completado'], 'integer'],
            [['desde', 'hasta', 'nivel_educativo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Educacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'institucion_educativa_id' => $this->institucion_educativa_id,
            'con_discapacidad_id' => $this->con_discapacidad_id,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'completado' => $this->completado,
        ]);

        $query->andFilterWhere(['like', 'nivel_educativo', $this->nivel_educativo]);

        return $dataProvider;
    }
}
