<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_vinculo".
 *
 * @property int $id
 * @property string|null $nombre
 *
 * @property Vinculo[] $vinculos
 */
class TipoVinculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_vinculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Vinculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVinculos()
    {
        return $this->hasMany(Vinculo::className(), ['tipo_vinculo_id' => 'id']);
    }
}
