<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vinculo".
 *
 * @property int $id
 * @property int|null $persona_a_id
 * @property int|null $persona_b_id
 * @property int|null $tipo_vinculo_id
 *
 * @property Persona $personaA
 * @property Persona $personaB
 * @property TipoVinculo $tipoVinculo
 */
class Vinculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vinculo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['persona_a_id', 'persona_b_id', 'tipo_vinculo_id'], 'integer'],
            [['persona_a_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_a_id' => 'id']],
            [['persona_b_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_b_id' => 'id']],
            [['tipo_vinculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => TipoVinculo::className(), 'targetAttribute' => ['tipo_vinculo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'persona_a_id' => 'Persona A ID',
            'persona_b_id' => 'Persona B ID',
            'tipo_vinculo_id' => 'Tipo Vinculo ID',
        ];
    }

    /**
     * Gets query for [[PersonaA]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaA()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_a_id']);
    }

    /**
     * Gets query for [[PersonaB]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonaB()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_b_id']);
    }

    /**
     * Gets query for [[TipoVinculo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoVinculo()
    {
        return $this->hasOne(TipoVinculo::className(), ['id' => 'tipo_vinculo_id']);
    }
}
