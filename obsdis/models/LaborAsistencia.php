<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "labor_asistencia".
 *
 * @property int $id
 * @property int|null $asistente_id
 * @property int|null $con_discapacidad_id
 * @property string|null $desde
 * @property string|null $hasta
 * @property int|null $formal
 * @property int|null $con_residencia
 *
 * @property Asistente $asistente
 * @property ConDiscapacidad $conDiscapacidad
 */
class LaborAsistencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labor_asistencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asistente_id', 'con_discapacidad_id'], 'integer'],
            [['con_residencia', 'formal'], 'boolean'],
            [['desde', 'hasta'], 'date',
             'format' => 'yyyy-m-dd',
            ],
            [['asistente_id'], 'exist',
             'skipOnError' => true,
             'targetClass' => Asistente::className(),
             'targetAttribute' => ['asistente_id' => 'id']],
            [['con_discapacidad_id'], 'exist',
             'skipOnError' => true,
             'targetClass' => ConDiscapacidad::className(),
             'targetAttribute' => ['con_discapacidad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'asistente_id' => 'Asistente ID',
            'con_discapacidad_id' => 'Con Discapacidad ID',
            'desde' => 'Fecha en que comenzó la labor de asistencia',
            'hasta' => 'Fecha en que terminó la labor de asistencia',
            'formal' => 'Formal',
            'con_residencia' => 'Con residencia',
        ];
    }

    /**
     * Gets query for [[Asistente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistente()
    {
        return $this->hasOne(Asistente::className(), ['id' => 'asistente_id']);
    }

    /**
     * Gets query for [[ConDiscapacidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConDiscapacidad()
    {
        return $this->hasOne(ConDiscapacidad::className(),
                       ['id' => 'con_discapacidad_id']);
    }
}
