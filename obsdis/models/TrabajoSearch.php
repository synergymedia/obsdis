<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trabajo;

/**
 * TrabajoSearch represents the model behind the search form of `app\models\Trabajo`.
 */
class TrabajoSearch extends Trabajo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'institucion_id', 'con_discapacidad_id', 'registrado'], 'integer'],
            [['rubro', 'grado_resp', 'desde', 'hasta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trabajo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'institucion_id' => $this->institucion_id,
            'con_discapacidad_id' => $this->con_discapacidad_id,
            'registrado' => $this->registrado,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
        ]);

        $query->andFilterWhere(['like', 'rubro', $this->rubro])
            ->andFilterWhere(['like', 'grado_resp', $this->grado_resp]);

        return $dataProvider;
    }
}
