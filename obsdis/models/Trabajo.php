<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajo".
 *
 * @property int $id
 * @property int|null $institucion_id
 * @property int|null $con_discapacidad_id
 * @property string|null $rubro
 * @property string|null $grado_resp
 * @property int|null $registrado
 * @property string|null $desde
 * @property string|null $hasta
 *
 * @property ConDiscapacidad $conDiscapacidad
 * @property Institucion $institucion
 */
class Trabajo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institucion_id', 'con_discapacidad_id', 'registrado'], 'integer'],
            [['desde', 'hasta'], 'safe'],
            [['rubro', 'grado_resp'], 'string', 'max' => 255],
            [['con_discapacidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConDiscapacidad::className(), 'targetAttribute' => ['con_discapacidad_id' => 'id']],
            [['institucion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Institucion::className(), 'targetAttribute' => ['institucion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institucion_id' => 'Institucion ID',
            'con_discapacidad_id' => 'Con Discapacidad ID',
            'rubro' => 'Rubro',
            'grado_resp' => 'Grado Resp',
            'registrado' => 'Registrado',
            'desde' => 'Desde',
            'hasta' => 'Hasta',
        ];
    }

    /**
     * Gets query for [[ConDiscapacidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConDiscapacidad()
    {
        return $this->hasOne(ConDiscapacidad::className(), ['id' => 'con_discapacidad_id']);
    }

    /**
     * Gets query for [[Institucion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstitucion()
    {
        return $this->hasOne(Institucion::className(), ['id' => 'institucion_id']);
    }
}
