<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ConDiscapacidad;

/**
 * ConDiscapacidadSearch represents the model behind the search form of `app\models\ConDiscapacidad`.
 */
class ConDiscapacidadSearch extends ConDiscapacidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'persona_id', 'recibe_ayuda', 'puede_trabajar', 'puede_estudiar', 'vive_solo'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ConDiscapacidad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'persona_id' => $this->persona_id,
            'recibe_ayuda' => $this->recibe_ayuda,
            'puede_trabajar' => $this->puede_trabajar,
            'puede_estudiar' => $this->puede_estudiar,
            'vive_solo' => $this->vive_solo,
        ]);

        return $dataProvider;
    }
}
