<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property int $id
 * @property int|null $familia_id
 * @property int|null $vive_en
 * @property int|null $lugar_nac
 * @property int|null $estado_civil_id
 * @property int|null $genero_id
 * @property int|null $barrio_id
 * @property string|null $nombres
 * @property string|null $apellidos
 * @property string|null $dni
 * @property string|null $cuit
 * @property string|null $fecha_nac
 * @property string|null $celular
 * @property string|null $email
 * @property string|null $calle_residencia
 * @property int|null $altura
 *
 * @property Asistente[] $asistentes
 * @property ConDiscapacidad[] $conDiscapacidads
 * @property Barrio $barrio
 * @property Ciudad $lugarNac
 * @property Ciudad $viveEn
 * @property EstadoCivil $estadoCivil
 * @property Familia $familia
 * @property Genero $genero
 * @property Vinculo[] $vinculos
 * @property Vinculo[] $vinculos0
 */
class Persona extends \yii\db\ActiveRecord
{

    public $genero_str;
    public $verifyCode;

    const CREATING_SCENARIO = 'creating';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombres', 'apellidos', 'dni', 'email'], 'required',
             'message' => 'Debe completar este campo'],
            
            [['familia_id', 'vive_en', 'lugar_nac', 'estado_civil_id',
              'genero_id', 'barrio_id', 'altura'], 'integer'],
            [['fecha_nac'], 'date',
             'format' => 'yyyy-m-dd',
             // 'locale' => 'es_AR'
            ],
            [['nombres', 'apellidos', 'dni', 'cuit', 'celular', 'email',
              'calle_residencia'], 'string', 'max' => 255],
            [['email'], 'email',
             'message' => 'Debe escribir un correo electrónico válido.'],
            [['barrio_id'], 'exist', 'skipOnError' => true,
             'targetClass' => Barrio::className(),
             'targetAttribute' => ['barrio_id' => 'id']],
            [['lugar_nac'], 'exist', 'skipOnError' => true,
             'targetClass' => Ciudad::className(),
             'targetAttribute' => ['lugar_nac' => 'id']],
            [['vive_en'], 'exist', 'skipOnError' => true,
             'targetClass' => Ciudad::className(),
             'targetAttribute' => ['vive_en' => 'id']],
            [['estado_civil_id'], 'exist', 'skipOnError' => true,
             'targetClass' => EstadoCivil::className(),
             'targetAttribute' => ['estado_civil_id' => 'id']],
            [['familia_id'], 'exist', 'skipOnError' => true,
             'targetClass' => Familia::className(),
             'targetAttribute' => ['familia_id' => 'id']],
            [['genero_id'], 'exist', 'skipOnError' => true,
             'targetClass' => Genero::className(),
             'targetAttribute' => ['genero_id' => 'id']],
            
            ['verifyCode', 'captcha', 'on' => Persona::CREATING_SCENARIO],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'familia_id' => 'Familia',
            'vive_en' => 'Ciudad en la que vive actualmente ',
            'lugar_nac' => 'Lugar de nacimiento',
            'estado_civil_id' => 'Estado Civil',
            'genero_str' => 'Género',
            'genero_id' => 'Género',
            'barrio_id' => 'Barrio',
            'nombres' => 'Nombres',
            'apellidos' => 'Apellidos',
            'dni' => 'DNI',
            'cuit' => 'CUIT/CUIL',
            'fecha_nac' => 'Fecha de nacimiento',
            'celular' => 'Teléfono celular',
            'email' => 'E-mail',
            'calle_residencia' => 'Calle de residencia',
            'altura' => 'Altura de su residencia',
        ];
    }

    /**
     * Gets query for [[Asistentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistentes()
    {
        return $this->hasMany(Asistente::className(), ['persona_id' => 'id']);
    }

    /**
     * Gets query for [[ConDiscapacidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConDiscapacidads()
    {
        return $this->hasMany(ConDiscapacidad::className(), ['persona_id' => 'id']);
    }

    /**
     * Gets query for [[Barrio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBarrio()
    {
        return $this->hasOne(Barrio::className(), ['id' => 'barrio_id']);
    }

    /**
     * Gets query for [[LugarNac]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLugarNac()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'lugar_nac']);
    }

    /**
     * Gets query for [[ViveEn]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getViveEn()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'vive_en']);
    }

    /**
     * Gets query for [[EstadoCivil]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoCivil()
    {
        return $this->hasOne(EstadoCivil::className(), ['id' => 'estado_civil_id']);
    }

    /**
     * Gets query for [[Familia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFamilia()
    {
        return $this->hasOne(Familia::className(), ['id' => 'familia_id']);
    }

    /**
     * Gets query for [[Genero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenero()
    {
        return $this->hasOne(Genero::className(), ['id' => 'genero_id']);
    }

    /**
     * Gets query for [[Vinculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVinculos()
    {
        return $this->hasMany(Vinculo::className(), ['persona_a_id' => 'id']);
    }

    /**
     * Gets query for [[Vinculos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVinculos0()
    {
        return $this->hasMany(Vinculo::className(), ['persona_b_id' => 'id']);
    }
}
