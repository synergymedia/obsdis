<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cud".
 *
 * @property int $id
 * @property int|null $con_discapacidad_id
 * @property int|null $numero
 * @property string|null $vencimiento
 * @property int|null $disc_temp
 * @property string|null $descripcion
 *
 * @property ConDiscapacidad $conDiscapacidad
 */
class Cud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['con_discapacidad_id', 'numero'], 'integer'],
            [['disc_temp'], 'boolean'],
            [['vencimiento'], 'date',
             'format' => 'yyyy-m-dd',
            ],
            [['descripcion'], 'string'],
            [['con_discapacidad_id'], 'exist',
             'skipOnError' => true,
             'targetClass' => ConDiscapacidad::className(),
             'targetAttribute' => ['con_discapacidad_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'con_discapacidad_id' => 'Con Discapacidad ID',
            'numero' => 'Número',
            'vencimiento' => 'Vencimiento',
            'disc_temp' => 'Discapacidad temporal',
            'descripcion' => 'Descripción de su discapacidad',
        ];
    }

    /**
     * Gets query for [[ConDiscapacidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConDiscapacidad()
    {
        return $this->hasOne(ConDiscapacidad::className(),
                       ['id' => 'con_discapacidad_id']);
    }
}
