<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "educacion".
 *
 * @property int $id
 * @property int|null $institucion_educativa_id
 * @property int|null $con_discapacidad_id
 * @property string|null $desde
 * @property string|null $hasta
 * @property string|null $nivel_educativo
 * @property int|null $completado
 *
 * @property ConDiscapacidad $conDiscapacidad
 * @property InstitucionEducativa $institucionEducativa
 */
class Educacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'educacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institucion_educativa_id', 'con_discapacidad_id', 'completado'], 'integer'],
            [['desde', 'hasta'], 'safe'],
            [['nivel_educativo'], 'string', 'max' => 255],
            [['con_discapacidad_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConDiscapacidad::className(), 'targetAttribute' => ['con_discapacidad_id' => 'id']],
            [['institucion_educativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstitucionEducativa::className(), 'targetAttribute' => ['institucion_educativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institucion_educativa_id' => 'Institucion Educativa ID',
            'con_discapacidad_id' => 'Con Discapacidad ID',
            'desde' => 'Desde',
            'hasta' => 'Hasta',
            'nivel_educativo' => 'Nivel Educativo',
            'completado' => 'Completado',
        ];
    }

    /**
     * Gets query for [[ConDiscapacidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConDiscapacidad()
    {
        return $this->hasOne(ConDiscapacidad::className(), ['id' => 'con_discapacidad_id']);
    }

    /**
     * Gets query for [[InstitucionEducativa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstitucionEducativa()
    {
        return $this->hasOne(InstitucionEducativa::className(), ['id' => 'institucion_educativa_id']);
    }
}
