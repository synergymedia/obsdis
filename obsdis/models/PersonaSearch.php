<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Persona;

/**
 * PersonaSearch represents the model behind the search form of `app\models\Persona`.
 */
class PersonaSearch extends Persona
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'familia_id', 'vive_en', 'lugar_nac', 'estado_civil_id', 'genero_id', 'barrio_id', 'altura'], 'integer'],
            [['nombres', 'apellidos', 'dni', 'cuit', 'fecha_nac', 'celular', 'email', 'calle_residencia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Persona::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'familia_id' => $this->familia_id,
            'vive_en' => $this->vive_en,
            'lugar_nac' => $this->lugar_nac,
            'estado_civil_id' => $this->estado_civil_id,
            'genero_id' => $this->genero_id,
            'barrio_id' => $this->barrio_id,
            'fecha_nac' => $this->fecha_nac,
            'altura' => $this->altura,
        ]);

        $query->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'cuit', $this->cuit])
            ->andFilterWhere(['like', 'celular', $this->celular])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'calle_residencia', $this->calle_residencia]);

        return $dataProvider;
    }
}
