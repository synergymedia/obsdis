<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institucion_educativa".
 *
 * @property int $id
 * @property int|null $institucion_id
 *
 * @property Educacion[] $educacions
 * @property Institucion $institucion
 */
class InstitucionEducativa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institucion_educativa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institucion_id'], 'integer'],
            [['institucion_id'], 'exist', 'skipOnError' => true, 'targetClass' => Institucion::className(), 'targetAttribute' => ['institucion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institucion_id' => 'Institucion ID',
        ];
    }

    /**
     * Gets query for [[Educacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEducacions()
    {
        return $this->hasMany(Educacion::className(), ['institucion_educativa_id' => 'id']);
    }

    /**
     * Gets query for [[Institucion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstitucion()
    {
        return $this->hasOne(Institucion::className(), ['id' => 'institucion_id']);
    }
}
