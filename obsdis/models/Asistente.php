<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asistente".
 *
 * @property int $id
 * @property int|null $persona_id
 *
 * @property Persona $persona
 * @property LaborAsistencia[] $laborAsistencias
 */
class Asistente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asistente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['persona_id'], 'integer'],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'persona_id' => 'Persona ID',
        ];
    }

    /**
     * Gets query for [[Persona]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }

    /**
     * Gets query for [[LaborAsistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLaborAsistencias()
    {
        return $this->hasMany(LaborAsistencia::className(), ['asistente_id' => 'id']);
    }
}
