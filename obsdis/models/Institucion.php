<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institucion".
 *
 * @property int $id
 * @property string|null $nombre
 *
 * @property InstitucionEducativa[] $institucionEducativas
 * @property Trabajo[] $trabajos
 */
class Institucion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institucion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[InstitucionEducativas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstitucionEducativas()
    {
        return $this->hasMany(InstitucionEducativa::className(), ['institucion_id' => 'id']);
    }

    /**
     * Gets query for [[Trabajos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajos()
    {
        return $this->hasMany(Trabajo::className(), ['institucion_id' => 'id']);
    }
}
