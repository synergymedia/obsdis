<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "con_discapacidad".
 *
 * @property int $id
 * @property int|null $persona_id
 * @property int|null $recibe_ayuda
 * @property int|null $puede_trabajar
 * @property int|null $puede_estudiar
 * @property int|null $vive_solo
 *
 * @property Persona $persona
 * @property Cud[] $cuds
 * @property Educacion[] $educacions
 * @property LaborAsistencia[] $laborAsistencias
 * @property Trabajo[] $trabajos
 */
class ConDiscapacidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'con_discapacidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['persona_id', 'recibe_ayuda', 'puede_trabajar', 'puede_estudiar', 'vive_solo'], 'integer'],
            [['persona_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['persona_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'persona_id' => 'Persona ID',
            'recibe_ayuda' => 'Recibe Ayuda',
            'puede_trabajar' => 'Puede Trabajar',
            'puede_estudiar' => 'Puede Estudiar',
            'vive_solo' => 'Vive Solo',
        ];
    }

    /**
     * Gets query for [[Persona]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['id' => 'persona_id']);
    }

    /**
     * Gets query for [[Cuds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuds()
    {
        return $this->hasMany(Cud::className(), ['con_discapacidad_id' => 'id']);
    }

    /**
     * Gets query for [[Educacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEducacions()
    {
        return $this->hasMany(Educacion::className(), ['con_discapacidad_id' => 'id']);
    }

    /**
     * Gets query for [[LaborAsistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLaborAsistencias()
    {
        return $this->hasMany(LaborAsistencia::className(), ['con_discapacidad_id' => 'id']);
    }

    /**
     * Gets query for [[Trabajos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajos()
    {
        return $this->hasMany(Trabajo::className(), ['con_discapacidad_id' => 'id']);
    }
}
