<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "familia".
 *
 * @property int $id
 * @property int|null $cant_integrantes
 *
 * @property Persona[] $personas
 */
class Familia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'familia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cant_integrantes'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cant_integrantes' => 'Cant Integrantes',
        ];
    }

    /**
     * Gets query for [[Personas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonas()
    {
        return $this->hasMany(Persona::className(), ['familia_id' => 'id']);
    }
}
