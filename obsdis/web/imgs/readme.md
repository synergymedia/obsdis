# Orígenes de las imágenes
- disability-symbols.png desde [etalpharma.com](https://etalpharma.com/en-el-dia-mundial-de-las-personas-con-discapacidad-celebramos-la-apertura-del-primer-supermercado-en-cataluna-gestionado-integramente-por-personas-discapacitadas/)
- chica-silla-ruedas-vexels.svg obtenido desde [Chica silla de ruedas ruddiness persona con discapacidad plana |   Designed by Vexels.com](https://www.vexels.com/vectores/vista-previa/175933/chica-silla-de-ruedas-ruddiness-persona-con-discapacidad-plana)
