// Generated by CoffeeScript 2.4.1
(function() {
  var assign_events, initialize_widgets, show_input_type;

  show_input_type = function(state) {
    console.log('show_input:', state);
    if (state) {
      // True => con discapacidad
      $(".asistencia").hide();
      return $(".discapacidad").show();
    } else {
      // False => asiste
      $(".asistencia").show();
      return $(".discapacidad").hide();
    }
  };

  assign_events = function() {
    return $("[name=tipo_persona]").on('switchChange.bootstrapSwitch', function(event, state) {
      return show_input_type(state);
    });
  };

  initialize_widgets = function() {
    var input_state;
    input_state = $("[name=tipo_persona]")[0].checked;
    show_input_type(input_state);
    return console.log('persona-new initialized');
  };

  $(document).ready(function() {
    assign_events();
    return initialize_widgets();
  });

}).call(this);
