show_input_type = (state) ->
    console.log 'show_input:', state
    if state
        # True => con discapacidad
        $(".asistencia").hide()
        $(".discapacidad").show();
    else
        # False => asiste
        $(".asistencia").show()
        $(".discapacidad").hide();

assign_events = () ->
    $("[name=tipo_persona]").on 'switchChange.bootstrapSwitch',
        (event, state) ->
            show_input_type state

initialize_widgets = () ->
    input_state = $("[name=tipo_persona]")[0].checked
    show_input_type input_state
    console.log 'persona-new initialized'

$(document).ready () ->
    assign_events()
    initialize_widgets()
