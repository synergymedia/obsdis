<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cud}}`.
 */
class m200615_231856_create_cud_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cud}}', [
            'id' => $this->primaryKey(),
            'con_discapacidad_id' => $this->integer(),
            'numero' => $this->integer(),
            'vencimiento' => $this->date(),
            'disc_temp' => $this->boolean(),
            'descripcion' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cud}}');
    }
}
