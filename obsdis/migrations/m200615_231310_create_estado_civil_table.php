<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%estado_civil}}`.
 */
class m200615_231310_create_estado_civil_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%estado_civil}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%estado_civil}}');
    }
}
