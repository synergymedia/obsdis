<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%familia}}`.
 */
class m200615_231401_create_familia_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%familia}}', [
            'id' => $this->primaryKey(),
            'cant_integrantes' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%familia}}');
    }
}
