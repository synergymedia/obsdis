<?php

use yii\db\Migration;

/**
 * Class m200620_191227_add_fk_to_con_discapacidad
 */
class m200620_191227_add_fk_to_con_discapacidad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-con_discapacidad-persona',
            '{{%con_discapacidad}}', 'persona_id',
            '{{%persona}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-con_discapacidad-persona',
            '{{%con_discapacidad}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191227_add_fk_to_con_discapacidad cannot be reverted.\n";

        return false;
    }
    */
}
