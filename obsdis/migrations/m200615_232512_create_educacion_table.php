<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%educacion}}`.
 */
class m200615_232512_create_educacion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%educacion}}', [
            'id' => $this->primaryKey(),
            'institucion_educativa_id' => $this->integer(),
            'con_discapacidad_id' => $this->integer(),
            'desde' => $this->date(),
            'hasta' => $this->date(),
            'nivel_educativo' => $this->string(),
            'completado' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%educacion}}');
    }
}
