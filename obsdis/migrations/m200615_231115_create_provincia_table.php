<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%provincia}}`.
 */
class m200615_231115_create_provincia_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%provincia}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%provincia}}');
    }
}
