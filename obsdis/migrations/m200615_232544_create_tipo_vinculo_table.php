<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tipo_vinculo}}`.
 */
class m200615_232544_create_tipo_vinculo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tipo_vinculo}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tipo_vinculo}}');
    }
}
