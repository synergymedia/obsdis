<?php

use yii\db\Migration;

/**
 * Class m200620_182502_add_fk_to_ciudad
 */
class m200620_182502_add_fk_to_ciudad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-ciudad-provincia',
            '{{%ciudad}}', 'provincia_id',
            '{{%provincia}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-ciudad-provincia',
            '{{%ciudad}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_182502_add_fk_to_ciudad cannot be reverted.\n";

        return false;
    }
    */
}
