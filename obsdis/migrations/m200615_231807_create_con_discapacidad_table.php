<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%con_discapacidad}}`.
 */
class m200615_231807_create_con_discapacidad_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%con_discapacidad}}', [
            'id' => $this->primaryKey(),
            'persona_id' => $this->integer(),
	    'recibe_ayuda' => $this->boolean(),
	    'puede_trabajar' => $this->boolean(),
	    'puede_estudiar' => $this->boolean(),
	    'vive_solo' => $this->boolean(),	    
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%con_discapacidad}}');
    }
}
