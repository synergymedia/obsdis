<?php

use yii\db\Migration;

/**
 * Class m200620_191943_add_fks_to_trabajo
 */
class m200620_191943_add_fks_to_trabajo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-trabajo-institucion',
            '{{%trabajo}}', 'institucion_id',
            '{{%institucion}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-trabajo-con_discapacidad',
            '{{%trabajo}}', 'con_discapacidad_id',
            '{{%con_discapacidad}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-trabajo-institucion',
            '{{%trabajo}}'
        );
        $this->dropForeignKey(
            'fk-trabajo-con_discapacidad',
            '{{%trabajo}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191943_add_fks_to_trabajo cannot be reverted.\n";

        return false;
    }
    */
}
