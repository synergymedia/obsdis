<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%institucion}}`.
 */
class m200615_232224_create_institucion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%institucion}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%institucion}}');
    }
}
