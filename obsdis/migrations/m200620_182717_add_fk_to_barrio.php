<?php

use yii\db\Migration;

/**
 * Class m200620_182717_add_fk_to_barrio
 */
class m200620_182717_add_fk_to_barrio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-barrio-ciudad',
            '{{%barrio}}', 'ciudad_id',
            '{{%ciudad}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-barrio-ciudad',
            '{{%barrio}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_182717_add_fk_to_barrio cannot be reverted.\n";

        return false;
    }
    */
}
