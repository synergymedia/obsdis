<?php

use yii\db\Migration;

/**
 * Class m200620_182901_add_fks_to_persona
 */
class m200620_182901_add_fks_to_persona extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-persona-familia',
            '{{%persona}}', 'familia_id',
            '{{%familia}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-persona-ciudad-vive_en',
            '{{%persona}}', 'vive_en',
            '{{%ciudad}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-persona-ciudad-lugar_nac',
            '{{%persona}}', 'lugar_nac',
            '{{%ciudad}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-persona-estado_civil',
            '{{%persona}}', 'estado_civil_id',
            '{{%estado_civil}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-persona-genero',
            '{{%persona}}', 'genero_id',
            '{{%genero}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-persona-barrio',
            '{{%persona}}', 'barrio_id',
            '{{%barrio}}', 'id',
            'RESTRICT', 'CASCADE');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-persona-familia',
            '{{%persona}}'
        );
        $this->dropForeignKey(
            'fk-persona-ciudad-vive_en',
            '{{%persona}}'
        );
        $this->dropForeignKey(
            'fk-persona-ciudad-lugar_nac',
            '{{%persona}}'
        );
        $this->dropForeignKey(
            'fk-persona-estado_civil',
            '{{%persona}}'
        );
        $this->dropForeignKey(
            'fk-persona-genero',
            '{{%persona}}'
        );
        $this->dropForeignKey(
            'fk-persona-barrio',
            '{{%persona}}'
        );
        
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_182901_add_fks_to_persona cannot be reverted.\n";

        return false;
    }
    */
}
