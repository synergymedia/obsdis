<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%institucion_educativa}}`.
 */
class m200615_232242_create_institucion_educativa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%institucion_educativa}}', [
            'id' => $this->primaryKey(),
            'institucion_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%institucion_educativa}}');
    }
}
