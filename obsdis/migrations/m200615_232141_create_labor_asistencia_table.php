<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%labor_asistencia}}`.
 */
class m200615_232141_create_labor_asistencia_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%labor_asistencia}}', [
            'id' => $this->primaryKey(),
            'asistente_id' => $this->integer(),
            'con_discapacidad_id' => $this->integer(),
            'desde' => $this->date(),
            'hasta' => $this->date(),
            'formal' => $this->boolean(),
            'con_residencia' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%labor_asistencia}}');
    }
}
