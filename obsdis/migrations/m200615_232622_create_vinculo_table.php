<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vinculo}}`.
 */
class m200615_232622_create_vinculo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vinculo}}', [
            'id' => $this->primaryKey(),
            'persona_a_id' => $this->integer(),
            'persona_b_id' => $this->integer(),
	    'tipo_vinculo_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vinculo}}');
    }
}
