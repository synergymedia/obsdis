<?php

use yii\db\Migration;

/**
 * Class m200620_191116_add_fk_to_asistente
 */
class m200620_191116_add_fk_to_asistente extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-asistente-persona',
            '{{%asistente}}', 'persona_id',
            '{{%persona}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-asistente-persona',
            '{{%asistente}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191116_add_fk_to_asistente cannot be reverted.\n";

        return false;
    }
    */
}
