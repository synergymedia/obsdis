<?php

use yii\db\Migration;

/**
 * Class m200620_192226_add_fks_to_vinculo
 */
class m200620_192226_add_fks_to_vinculo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-vinculo-persona-a',
            '{{%vinculo}}', 'persona_a_id',
            '{{%persona}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-vinculo-persona-b',
            '{{%vinculo}}', 'persona_b_id',
            '{{%persona}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-vinculo-tipo_vinculo',
            '{{%vinculo}}', 'tipo_vinculo_id',
            '{{%tipo_vinculo}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-vinculo-persona-a',
            '{{%vinculo}}'
        );
        $this->dropForeignKey(
            'fk-vinculo-persona-b',
            '{{%vinculo}}'
        );
        $this->dropForeignKey(
            'fk-vinculo-tipo_vinculo',
            '{{%vinculo}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_192226_add_fks_to_vinculo cannot be reverted.\n";

        return false;
    }
    */
}
