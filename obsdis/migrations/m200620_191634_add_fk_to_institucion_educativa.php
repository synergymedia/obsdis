<?php

use yii\db\Migration;

/**
 * Class m200620_191634_add_fk_to_institucion_educativa
 */
class m200620_191634_add_fk_to_institucion_educativa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-institucion_educativa-institucion',
            '{{%institucion_educativa}}', 'institucion_id',
            '{{%institucion}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-institucion_educativa-institucion',
            '{{%institucion_educativa}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191634_add_fk_to_institucion_educativa cannot be reverted.\n";

        return false;
    }
    */
}
