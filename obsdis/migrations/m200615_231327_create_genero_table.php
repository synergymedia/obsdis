<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%genero}}`.
 */
class m200615_231327_create_genero_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%genero}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%genero}}');
    }
}
