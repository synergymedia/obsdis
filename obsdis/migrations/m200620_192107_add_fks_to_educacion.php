<?php

use yii\db\Migration;

/**
 * Class m200620_192107_add_fks_to_educacion
 */
class m200620_192107_add_fks_to_educacion extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-educacion-institucion_educativa',
            '{{%educacion}}', 'institucion_educativa_id',
            '{{%institucion_educativa}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-educacion-con_discapacidad',
            '{{%educacion}}', 'con_discapacidad_id',
            '{{%con_discapacidad}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-educacion-institucion_educativa',
            '{{%educacion}}'
        );
        $this->dropForeignKey(
            'fk-educacion-con_discapacidad',
            '{{%educacion}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_192107_add_fks_to_educacion cannot be reverted.\n";

        return false;
    }
    */
}
