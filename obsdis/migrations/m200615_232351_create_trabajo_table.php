<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%trabajo}}`.
 */
class m200615_232351_create_trabajo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%trabajo}}', [
            'id' => $this->primaryKey(),
            'institucion_id' => $this->integer(),
            'con_discapacidad_id' => $this->integer(),
            'rubro' => $this->string(),
            'grado_resp' => $this->string(),
            'registrado' => $this->boolean(),
            'desde' => $this->date(),
            'hasta' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%trabajo}}');
    }
}
