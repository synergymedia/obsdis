<?php

use yii\db\Migration;

/**
 * Class m200620_191337_add_fk_to_cud
 */
class m200620_191337_add_fk_to_cud extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-cud-con_discapacidad',
            '{{%cud}}', 'con_discapacidad_id',
            '{{%con_discapacidad}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-cud-con_discapacidad',
            '{{%cud}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191337_add_fk_to_cud cannot be reverted.\n";

        return false;
    }
    */
}
