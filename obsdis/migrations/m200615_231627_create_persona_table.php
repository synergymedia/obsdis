<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%persona}}`.
 */
class m200615_231627_create_persona_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%persona}}', [
            'id' => $this->primaryKey(),
            'familia_id' => $this->integer(),
            'vive_en' => $this->integer(),
            'lugar_nac' => $this->integer(),
            'estado_civil_id' => $this->integer(),
            'genero_id' => $this->integer(),
            'barrio_id' => $this->integer(),
            'nombres' => $this->string(),
            'apellidos' => $this->string(),
            'dni' => $this->string(),
            'cuit' => $this->string(),
            'fecha_nac' => $this->date(),
            'celular' => $this->string(),
            'email' => $this->string(),
            'calle_residencia' => $this->string(),
            'altura' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%persona}}');
    }
}
