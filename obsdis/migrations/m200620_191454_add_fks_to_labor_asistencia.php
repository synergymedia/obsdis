<?php

use yii\db\Migration;

/**
 * Class m200620_191454_add_fks_to_labor_asistencia
 */
class m200620_191454_add_fks_to_labor_asistencia extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-labor_asistencia-asistente',
            '{{%labor_asistencia}}', 'asistente_id',
            '{{%asistente}}', 'id',
            'RESTRICT', 'CASCADE');
        $this->addForeignKey(
            'fk-labor_asistencia-con_discapacidad',
            '{{%labor_asistencia}}', 'con_discapacidad_id',
            '{{%con_discapacidad}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-labor_asistencia-asistente',
            '{{%labor_asistencia}}'
        );
        $this->dropForeignKey(
            'fk-labor_asistencia-con_discapacidad',
            '{{%labor_asistencia}}'
        );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200620_191454_add_fks_to_labor_asistencia cannot be reverted.\n";

        return false;
    }
    */
}
