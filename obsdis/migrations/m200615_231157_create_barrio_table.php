<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%barrio}}`.
 */
class m200615_231157_create_barrio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%barrio}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
            'ciudad_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%barrio}}');
    }
}
