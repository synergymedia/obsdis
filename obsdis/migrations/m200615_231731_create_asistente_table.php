<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%asistente}}`.
 */
class m200615_231731_create_asistente_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%asistente}}', [
            'id' => $this->primaryKey(),
            'persona_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%asistente}}');
    }
}
