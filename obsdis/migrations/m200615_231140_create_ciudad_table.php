<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ciudad}}`.
 */
class m200615_231140_create_ciudad_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ciudad}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string(),
            'provincia_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ciudad}}');
    }
}
