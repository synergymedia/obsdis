<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ConDiscapacidad */

$this->title = Yii::t('app', 'Create Con Discapacidad');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Con Discapacidads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="con-discapacidad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
