<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConDiscapacidad */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="con-discapacidad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'persona_id')->textInput() ?>

    <?= $form->field($model, 'recibe_ayuda')->textInput() ?>

    <?= $form->field($model, 'puede_trabajar')->textInput() ?>

    <?= $form->field($model, 'puede_estudiar')->textInput() ?>

    <?= $form->field($model, 'vive_solo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
