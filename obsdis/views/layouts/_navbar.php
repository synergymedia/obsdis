<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],

        ['label' => 'Gestionar',
         'options' => [
             'style' => 'background-color: #400'
         ],
         'visible' => (Yii::$app->user->can('Admin')),
         'items' => [
             ['label' => 'Persona', 'url' => ['/persona']],
             ['label' => 'Institución', 'url' => ['/institucion']],
             
             '<li class="divider"></li>',
             ['label' => 'Géneros', 'url' => ['/genero']],
             ['label' => 'Barrios', 'url' => ['/barrio']],
             ['label' => 'Ciudades', 'url' => ['/ciudad']],
             ['label' => 'Provincias', 'url' => ['/provincia']],
             ['label' => 'Estados civiles', 'url' => ['/estado-civil']],
             ['label' => 'Labores de asistencias',
              'url' => ['/labor-asistencia']],
             ['label' => 'Tipos de vínculos', 'url' => ['/tipo-vinculo']],
             '<li class="divider"></li>',
             ['label' => 'Usuarios y roles', 'url' => ['/user/admin']],
         ],

         
        ],
        
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
        Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/user/security/login']]
        ) : (
            '<li>'
          . Html::beginForm(['/user/security/logout'], 'post')
          . Html::submitButton(
              'Logout (' . Yii::$app->user->identity->username . ')',
              ['class' => 'btn btn-link logout']
          )
          . Html::endForm()
          . '</li>'
        )
    ],
]);
NavBar::end();
?>
