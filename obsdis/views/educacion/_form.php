<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Educacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'institucion_educativa_id')->textInput() ?>

    <?= $form->field($model, 'con_discapacidad_id')->textInput() ?>

    <?= $form->field($model, 'desde')->textInput() ?>

    <?= $form->field($model, 'hasta')->textInput() ?>

    <?= $form->field($model, 'nivel_educativo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'completado')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
