<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Educacion */

$this->title = Yii::t('app', 'Create Educacion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Educacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
