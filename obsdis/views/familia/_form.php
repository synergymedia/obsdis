<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Familia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="familia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cant_integrantes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
