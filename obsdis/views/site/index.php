<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

use app\assets\IndexPageAsset;
IndexPageAsset::register($this);

$this->title = 'Observatorio';
?>
<section class="main dark">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="texto" data-aos="fade-right">
          <div class="jumbotron">
            <h1>Observatorio de la Discapacidad</h1>
            <p>Somos un equipo dedicado a...</p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div data-aos="fade-left">
          <img class="img-responsive img-circle"
               src="imgs/disability-symbols.png">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="text-center"> 
  <div class="container registro">
    <div class="row">
      <div class="col-md-6">
        <div data-aos="fade-right">
          <img class="img-responsive img-rounded"
               src="imgs/chica-silla-ruedas-vexels.svg">
        </div>
      </div>
      <div class="col-md-6">
        <div data-aos="fade-left">
          <div class="jumbotron">
            <p>Para acceder a información y beneficios. </p>
            <?= Html::a('Registrarse', ['persona/create'], [
                'class' => 'btn btn-success btn-lg'
            ]) ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="text-center dark">
  <div class="container" data-aos="fade-in">
    <div class="jumbotron">
      <h1>Acerca de nosotros</h1>
      <p>...</p>
    </div>
  </div>
</section>
