<?php

/* @var $this yii\web\View */
/* @var $model app\models\Persona */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\datepicker\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;
use kartik\widgets\SwitchInput;
use app\assets\PersonaNewAsset;
PersonaNewAsset::register($this);

use app\models\EstadoCivil;
use app\models\Genero;
use app\models\Barrio;
use app\models\Ciudad;
use app\models\Persona;

$generos = Genero::find()->select(['nombre'])
                 ->column();
?>

<div class="persona-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= Html::label(
      '¿Es usted una persona con discapacidad o asiste a una persona?'
  ) ?>
  <?= SwitchInput::widget([
      'name' => 'tipo_persona',
      'value' => true,
      'options' => [
          'data-type' => 'tipo-persona-switch',
      ],
      'pluginOptions' => [
          'size' => 'large',
          'onText' => 'Con discapacidad',
          'offText' => 'Asisto a una persona',
      ],
  ]) ?>

  <div class="discapacidad">
    <div class="alert alert-info">
      Por favor, tenga a mano su Certificado Único de Discapacidad (CUD).
    </div>
  </div>
  <div class="asistencia">
    <div class="alert alert-warning">
      La persona a la que asiste debe estar registrada en el sistema
      con anterioridad.

      Dicho registro se realiza con este mismo formulario seleccionando
      "Con discapacidad" en la pregunta de arriba.
    </div>
    <div class="alert alert-info">
      Le solicitaremos el número de DNI de la persona que usted está asistiendo.
      Téngalo a mano previo a completar el formulario.
    </div>
  </div>

  <p>Complete los siguientes campos con sus datos personales</p>
  
  <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'cuit')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'fecha_nac')->widget(
      DatePicker::className(), [
          'clientOptions' => [
              'autoclose' => true,
              'format' => 'dd-m-yyyy'
          ]
  ]); ?>
  
  <?= $form->field($model, 'familia_id')->HiddenInput()->label(false) ?>

  <?= $form->field($model, 'lugar_nac')->dropdownList(
      Ciudad::find()->select(['nombre'])
            ->indexBy('id')
            ->column()) ?>

  <?= $form->field($model, 'estado_civil_id')->dropdownList(
      EstadoCivil::find()->select(['nombre'])
                 ->indexBy('id')
                 ->column()) ?>

  <?= $form->field($model, 'genero_str')->widget(
      SelectizeDropDownList::className(), [
          'items' => $generos,
          'clientOptions' => [
              'create' => true,
          ],
  ])?>
  <p class="help-block">Si no encuentra el género que lo represente,
    puede ingresar un valor nuevo.</p>

  <h1>Contacto</h1>

  <p>Para mantenernos informados, requerimos de sus datos de contacto.</p>
  
  <?= $form->field($model, 'celular')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

  <h1>Lugar de residencia</h1>

  <?= $form->field($model, 'barrio_id')->dropdownList(
      Barrio::find()->select(['nombre'])
            ->indexBy('id')
            ->column()) ?>
  
  <?= $form->field($model, 'vive_en')->dropdownList(
      Ciudad::find()->select(['nombre'])
            ->indexBy('id')
            ->column()) ?>
  
  <div class="row ">
    <div class="col-md-6">
      <?= $form->field($model, 'calle_residencia')
             ->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6">
      <?= $form->field($model, 'altura')->textInput() ?>      
    </div>
  </div>

  
  <div class="asistencia">
    <h1>De la Asistencia</h1>
    <?= $this->render('_asistente', [
        'form' => $form,
        'model' => $labor_asis,
    ]) ?>
  </div>
  <div class="discapacidad">
    <h1>De su discapacidad</h1>
    <?= $this->render('_cud', [
        'form' => $form,
        'model' => $cud,
    ]) ?>
  </div>

  <?php
  if ($model->scenario == Persona::CREATING_SCENARIO){
      echo $form->field($model, 'verifyCode')
               ->widget(yii\captcha\Captcha::className());
      
  }
  ?>
  
  <div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Guardar'),
                         ['class' => 'btn btn-success']) ?>
  </div>
  
  
  <?php ActiveForm::end(); ?>

</div>
