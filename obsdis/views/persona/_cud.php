<?php

/**
   @var $model app\models\Cud
   @var $form yii\widgets\ActiveForm
   @var $this pii\web\View
 */

use dosamigos\datepicker\DatePicker;

?>

<p>Ingrese los datos que aparecen en su Certificado Único de Discapacidad (CUD)</p>

<?= $form->field($model, 'numero')->textInput() ?>

<?= $form->field($model, 'vencimiento')->widget(
    DatePicker::className(), [
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-m-yyyy'
        ]
]) ?>

<?= $form->field($model, 'disc_temp')->checkbox() ?>

<?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
