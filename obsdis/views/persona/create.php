<?php

/* @var $this yii\web\View */
/* @var $model app\models\Persona */
/* @var $cud app\models\Cud */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Registrarse');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persona-create">

  <h1><?= Html::encode($this->title) ?></h1>
  
  <?= $this->render('_form', [
      'model' => $model,
      'cud' => $cud,
      'labor_asis' => $labor_asis,
  ]) ?>

</div>
