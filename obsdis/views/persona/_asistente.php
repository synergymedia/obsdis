<?php

use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;

?>

<div class="alert alert-warning">
  La persona a la que asiste debe estar registrada para completar este campo.
</div>

<?= Html::label('Indique el DNI de la persona a la que asiste') ?>
<?= Html::textInput('dni_disc', '', [
    'class' => 'form-control',
]) ?>

<?= $form->field($model, 'desde')->widget(
    DatePicker::className(), [
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-m-yyyy'
        ]
]) ?>

<?= $form->field($model, 'con_residencia')->checkbox() ?>
