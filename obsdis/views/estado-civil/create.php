<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoCivil */

$this->title = Yii::t('app', 'Create Estado Civil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estado Civils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-civil-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
