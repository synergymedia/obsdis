<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vinculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vinculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'persona_a_id')->textInput() ?>

    <?= $form->field($model, 'persona_b_id')->textInput() ?>

    <?= $form->field($model, 'tipo_vinculo_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
