<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InstitucionEducativa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institucion-educativa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'institucion_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
