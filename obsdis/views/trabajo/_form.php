<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trabajo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'institucion_id')->textInput() ?>

    <?= $form->field($model, 'con_discapacidad_id')->textInput() ?>

    <?= $form->field($model, 'rubro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grado_resp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registrado')->textInput() ?>

    <?= $form->field($model, 'desde')->textInput() ?>

    <?= $form->field($model, 'hasta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
