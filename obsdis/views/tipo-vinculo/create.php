<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TipoVinculo */

$this->title = Yii::t('app', 'Create Tipo Vinculo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Vinculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-vinculo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
