<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cud */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cud-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'con_discapacidad_id')->textInput() ?>

    <?= $form->field($model, 'numero')->textInput() ?>

    <?= $form->field($model, 'vencimiento')->textInput() ?>

    <?= $form->field($model, 'disc_temp')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
