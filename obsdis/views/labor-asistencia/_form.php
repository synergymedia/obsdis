<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LaborAsistencia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="labor-asistencia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'asistente_id')->textInput() ?>

    <?= $form->field($model, 'con_discapacidad_id')->textInput() ?>

    <?= $form->field($model, 'desde')->textInput() ?>

    <?= $form->field($model, 'hasta')->textInput() ?>

    <?= $form->field($model, 'formal')->textInput() ?>

    <?= $form->field($model, 'con_residencia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
