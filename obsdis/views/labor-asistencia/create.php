<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LaborAsistencia */

$this->title = Yii::t('app', 'Create Labor Asistencia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Labor Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="labor-asistencia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
