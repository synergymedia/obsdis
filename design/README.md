# Licencia
![CC-By-SA logo](https://licensebuttons.net/l/by-sa/4.0/88x31.png)

Estos diseños se encuentran bajo la [licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional](http://creativecommons.org/licenses/by-sa/4.0/).
